#include "UsefulFunctions.h"
#include <math.h>

#define degToRad 0.017453293f

namespace UsefulFunctions{
	// works off the premise that at 0 degrees we don't want to move to the left or right what so
	// ever (hence forward co-ord uses cos and sideCoord uses sin, cos at 0 = 1 and sin at 0 = 0
	// thus no sideward movement just forward like it should be!) forwardCoord also negates the value
	// as we consider (or at time of creation/use) the -Z to be "forward", direction multiplier for
	// noting which direction we wish to go regardless of angle, also means that if we wish to just
	// change the angle we can set it to 1 and it won't move forward just rotate, cos positive so we 
	// rotate to the right as the angle increases
	Vector3D moveForward(Vector3D vector, float direction, float angle)
	{
		return Vector3D( vector.x + direction * sin(degToRad * angle), vector.y, 
			vector.z - direction * cos(degToRad * angle));
	}

	// same as above except reversed
	Vector3D moveRight(Vector3D vector, float direction, float angle)
	{
		return Vector3D( vector.x + direction * cos(degToRad * angle), vector.y,
			vector.z + direction * sin(degToRad * angle));
	}
}