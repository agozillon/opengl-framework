#version 330

in vec4 oColor;
in vec3 oNormal;
in vec3 oMSpacePosition;

out vec4 outputColor;

uniform vec3 lightPosition; // model space light position
uniform vec4 lightIntensity;
uniform vec4 ambient;

void main()
{    
    vec3 lightDir = normalize(lightPosition - oMSpacePosition);
    
    float cosAngIncidence = dot(normalize(oNormal), lightDir);
    cosAngIncidence = clamp(cosAngIncidence, 0, 1);
    
    outputColor = (oColor * lightIntensity * cosAngIncidence) + (oColor * ambient);
}