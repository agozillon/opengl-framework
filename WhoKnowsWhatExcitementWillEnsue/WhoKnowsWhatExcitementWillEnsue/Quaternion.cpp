#include "Quaternion.h"
#include <math.h>
#include <iostream>

#define degToRad 0.017453293f

void Quaternion::rotate(float x, float y, float z, float a)
{
	a = a * degToRad;
	i = x * sinf(a / 2);
	j = y * sinf(a / 2);
	k = z * sinf(a / 2);
	s = cosf(a / 2);
}

Quaternion Quaternion::operator*(Quaternion param)
{
	float temp[4];

	temp[0] = s * param.i + i * param.s + j * param.k - k * param.j;
	temp[1] = s * param.j + j * param.s + k * param.i - i * param.k;
	temp[2] = s * param.k + k * param.s + i * param.j - j * param.i;
	temp[3] = s * param.s - i * param.i - j * param.j - k * param.k;

	return Quaternion(temp[0], temp[1], temp[2], temp[3]);
}


void Quaternion::normalize()
{
	float divisor = sqrtf((i*i) + (j*j) + (k*k) + (s*s));

	i = (i / divisor);
	j = (j / divisor);
	k = (k / divisor);
	s = (s / divisor);
}

Matrix4x4 Quaternion::quaternionToMatrix()
{
	float temp[16];

	temp[0] = 1 - (2 * j * j) - (2 * k * k); 
	temp[1] = (2 * i * j) + (2 * s * k);
	temp[2] = (2 * i * k) - (2 * s * j);
	temp[3] = 0;
	temp[4] = (2 * i * j) - (2 * s * k);
	temp[5] = 1 - (2 * i * i) - (2 * k * k);
	temp[6] = (2 * j * k) + (2 * s * i);
	temp[7] = 0;
	temp[8] = (2 * i * k) + (2 * s * j);
	temp[9] = (2 * j * k) - (2 * s * i);
	temp[10] = 1 - (2 * i * i) - (2 * j * j);
	temp[11] = 0;
	temp[12] = 0;
	temp[13] = 0;
	temp[14] = 0;
	temp[15] = 1;

	return Matrix4x4(temp);
}