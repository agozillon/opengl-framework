#ifndef OBJMODELHOLDER_H
#define OBJMODELHOLDER_H
#include <string>
#include <vector>

namespace ObjModelLoader{

	void contentCheck(char * fname, std::vector<int> &indices, int  &indiceCount, std::vector<float> &vertices, std::vector<float> &normals, std::vector<float> &texCoords);
	
}

#endif
