#include "game.h"
#include <iostream>
#include <GL\glut.h>
#include "Matrix4x4.h"
#include "UsefulFunctions.h"
#include <glm/gtc/matrix_transform.hpp>	
#include "Vector3D.h"
#include "ObjModelLoader.h"

Game::Game()
{
//	globalMatricesBindingIndex = 0;
}


#define RIGHT_EXTENT 0.8f
#define LEFT_EXTENT -RIGHT_EXTENT
#define TOP_EXTENT 0.20f
#define MIDDLE_EXTENT 0.0f
#define BOTTOM_EXTENT -TOP_EXTENT
#define FRONT_EXTENT -1.25f
#define REAR_EXTENT -1.75f

#define GREEN_COLOR 0.75f, 0.75f, 1.0f, 1.0f
#define BLUE_COLOR 	0.0f, 0.5f, 0.0f, 1.0f
#define RED_COLOR 1.0f, 0.0f, 0.0f, 1.0f
#define GREY_COLOR 0.8f, 0.8f, 0.8f, 1.0f
#define BROWN_COLOR 0.5f, 0.5f, 0.0f, 1.0f


void Game::initializeVertexArrayObject()
{
	// creating a vertex array object and storing the handle in positionBufferObject
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	size_t colorDataOffset = sizeof(float) * 3 * 36;
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
	glEnableVertexAttribArray(0);
//	glEnableVertexAttribArray(1);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, normBufferObject);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 0, 0);
//	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*)colorDataOffset);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObject);
	glBindVertexArray(0);

		/* old VAO when required 2 seperate VAO's + data offsets when binding
	// binding buffer objects
	glBindBuffer(GL_ARRAY_BUFFER, positionBufferObject);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, colourBuffer);
	glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObject);
	glBindVertexArray(0);
	*/
}

void Game::initializeVertexBuffer()
{
	
	std::vector<float> normals;
	std::vector<int> indices;
	std::vector<float> vertices;
	std::vector<float> texCoords;

	ObjModelLoader::contentCheck("normCube.obj", indices, indiceCount, vertices, normals, texCoords);

	unsigned short * test;
	float * test1, * test2;
	test = new unsigned short[(int)indices.size()]; 
	test1 = new float[(int)vertices.size()]; 
	test2 = new float[(int)normals.size()];

	vertices.shrink_to_fit();
	indices.shrink_to_fit();
	normals.shrink_to_fit();

	for(int i = 0; i < indices.size(); i++)
	{
		test[i] = indices.data()[i];
	}
	
	for(int i = 0; i < vertices.size(); i++)
	{
		test1[i] = vertices.data()[i];
	//	std::cout << "Vertices " << test1[i] << std::endl;
	}
	
	for(int i = 1; i < normals.size()+1; i++)
	{
		test2[i-1] = normals.data()[i-1];
	/*
		if(i % 3 == 0)
		{
			std::cout << "Normals " << test2[i-3] << " " << test2[i-2] << " " << test2[i-1] << std::endl;
		}
	*/
	}	

	glUseProgram(theProgram);
	// creating a buffer object and storing the handle in positionBufferObject
	glGenBuffers(1, &vertexBufferObject);
	// binding the buffer object to the GL_ARRAY_BUFFER target 
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
	// passing the vertexPositions buffer data and size in bytes to the GPU/OpenGL context
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * vertices.size(), test1, GL_STREAM_DRAW);

	glGenBuffers(1, &normBufferObject);
	glBindBuffer(GL_ARRAY_BUFFER, normBufferObject);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * normals.size(), test2, GL_STATIC_DRAW);

	glGenBuffers(1, &indexBufferObject);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned short) * indices.size(), test, GL_STATIC_DRAW);
	

	eye = Vector3D(0.0f,0.0f,0.0f);
	at = Vector3D(0.0f, 0.0f, -2.0f);
	up = Vector3D(0.0f, 1.0f, 0.0f);
	lightIntensity = Vector3D(0.4f, 0.4f, 0.4f);
	lightPosition = Vector3D(10.0f, 10.0f, 0.0f);

	angle = 0;

	xRot = Quaternion();
	yRot = Quaternion();
	zRot = Quaternion();

	projectionMatrix = Matrix4x4();
	modelMatrix = Matrix4x4();
	viewMatrix = Matrix4x4();

	//globalMatricesUnifBlock = glGetUniformBlockIndex(theProgram, "GlobalMatrices");
	viewMatrixUnif = glGetUniformLocation(theProgram, "viewMatrix");
	perspectiveMatrixUnif = glGetUniformLocation(theProgram, "perspectiveMatrix");
	modelMatrixUnif = glGetUniformLocation(theProgram, "modelMatrix");
	lightIntensityUnif = glGetUniformLocation(theProgram, "lightIntensity");
	lightPositionUnif = glGetUniformLocation(theProgram, "lightPosition");
	ambientUnif = glGetUniformLocation(theProgram, "ambient");
	roughnessUnif = glGetUniformLocation(theProgram, "roughness");
	albedoUnif = glGetUniformLocation(theProgram, "albedo");
	projectionMatrix.projectionMatrix(90.0f, (float)(800 / 600), 1.0f, 90.0f);
	viewMatrix.viewMatrix(Vector3D(0.0f, 0.0f, 0.0f), Vector3D(0.0f, 1.0f, 0.0f), Vector3D(0.0f, 0.0f, -2.0f));
	


	float tempMat[16];
	projectionMatrix.getMat4x4(tempMat);
	glUniformMatrix4fv(perspectiveMatrixUnif, 1, GL_FALSE, tempMat);
	viewMatrix.getMat4x4(tempMat);
	glUniformMatrix4fv(viewMatrixUnif, 1, GL_FALSE, tempMat);
	/*
	glGenBuffers(1, &globalMatricesUBO);
	glBindBuffer(GL_UNIFORM_BUFFER, globalMatricesUBO);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(float) * 32, NULL, GL_STREAM_DRAW);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(float) * 16, tempMat);
	glBufferSubData(GL_UNIFORM_BUFFER, sizeof(float) * 16, sizeof(float) * 16, tempMat);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
	*/



//	glUniformBlockBinding(theProgram, globalMatricesUnifBlock,  globalMatricesBindingIndex);
//	glBindBufferRange(GL_UNIFORM_BUFFER, globalMatricesBindingIndex, globalMatricesUBO, 0, sizeof(float) * 32);
	
	glUniform4f(ambientUnif, 0.4, 0.4, 0.4, 0.0);
	glUniform4f(lightIntensityUnif, lightIntensity.x, lightIntensity.y, lightIntensity.z, 0.0);
	glUniform3f(lightPositionUnif, lightPosition.x, lightPosition.y, lightPosition.z);
	//glUniform1f(albedoUnif, 1.0);
	//glUniform1f(roughnessUnif, 1.0);

	// uniformID gotten via glGetUniformLocation, number of values to be changed(if it's an array that needs to be changed needs more values)
	//, column major or row major, valus to be passed

	glUseProgram(0);
	
	glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW);
	

	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LEQUAL);
	glDepthRange(0.0, 1.0);
}

std::string Game::fileToString(char * fileName)
{
	std::fstream fs;
	std::string tempS;

	fs.open(fileName);

	if (fs.is_open())
	{
		while(!fs.eof())
		{
			char c = fs.get();

			// stops appending if we get any file issues stops garbage characters
			// being appended.
			if(fs.good())
			tempS += c;
		}

		fs.close();
	}
	else
		std::cout << "file could not be opened";

//	std::cout << tempS << std::endl;

	return tempS;
}

void Game::reshape(int w, int h)
{
	projectionMatrix.projectionMatrix(90.0f, (float)(w / h), 1.0f, 10.0f);
    
	float tempMat[16];
	projectionMatrix.getMat4x4(tempMat);
	glUniformMatrix4fv(perspectiveMatrixUnif, 1, GL_FALSE, tempMat);
	
	/*
	glBindBuffer(GL_UNIFORM_BUFFER, globalMatricesUBO);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(float) * 16, tempMat);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    */
    
	glViewport(0, 0, (GLsizei) w, (GLsizei) h);
}

// compile a shader string into a shader object
GLuint Game::createShader(GLenum eShaderType, const std::string &strShaderFile)
{
	// create shader object
    GLuint shader = glCreateShader(eShaderType);

	// convert string to c style string
    const char *strFileData = strShaderFile.c_str();

    // compile string into shader object,  param 1 shader object id, param 2 number of strings going into shader
	// param 3 string to pass in, param 4 length of string (pass in null to assume that string is null terminated)
	glShaderSource(shader, 1, &strFileData, NULL);
    
	// compile the shader once strings are in the object
    glCompileShader(shader);
    
	// checking the status of the compiled shader (incase there were errors)
    GLint status;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

	// if false it failed get error message via GL info log, print it to screen
    if (status == GL_FALSE)
    {
		// getting length of log
        GLint infoLogLength;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);
        
		// putting log into a char string of log length + 1
        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetShaderInfoLog(shader, infoLogLength, NULL, strInfoLog);
        
		// get shader type to string via switch case
        const char *strShaderType = NULL;
        switch(eShaderType)
        {
			case GL_VERTEX_SHADER: strShaderType = "vertex"; break;
			case GL_GEOMETRY_SHADER: strShaderType = "geometry"; break;
			case GL_FRAGMENT_SHADER: strShaderType = "fragment"; break;
        }
        
		// print and then delete
        fprintf(stderr, "Compile failure in %s shader:\n%s\n", strShaderType, strInfoLog);
        delete[] strInfoLog;
	}

	return shader;
}

// link shader objects into a program object
GLuint Game::createProgram(const std::vector<GLuint> &shaderList)
{
	// create empty shader program
    GLuint program = glCreateProgram();
    
	// loop through passed in Shader Object List and attach to Program Object
    for(size_t iLoop = 0; iLoop < shaderList.size(); iLoop++)
    	glAttachShader(program, shaderList[iLoop]);
    
	// links the program together
    glLinkProgram(program);
    
	// getting status and error log of shader program
    GLint status;
    glGetProgramiv (program, GL_LINK_STATUS, &status);
    
	if (status == GL_FALSE)
    {
        GLint infoLogLength;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);
        
        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetProgramInfoLog(program, infoLogLength, NULL, strInfoLog);
        fprintf(stderr, "Linker failure: %s\n", strInfoLog);
    
		delete[] strInfoLog;
    }
    
	// detatching shader objects from program, doesn't affect shader program as already linked!
    for(size_t iLoop = 0; iLoop < shaderList.size(); iLoop++)
        glDetachShader(program, shaderList[iLoop]);

    return program;
}

void Game::initProgram(char * strVertShader, char * strFragShader)
{
	std::string vertString = fileToString(strVertShader);
	std::string fragString = fileToString(strFragShader);

	//vector shader list
    std::vector<GLuint> shaderList;
    
	// compiling the Vertex and Fragment shader strings
    shaderList.push_back(createShader(GL_VERTEX_SHADER, vertString));
    shaderList.push_back(createShader(GL_FRAGMENT_SHADER, fragString));
    
	// creating the shader program
    theProgram = createProgram(shaderList);

	// deleting the shader objects as they are no longer required
    std::for_each(shaderList.begin(), shaderList.end(), glDeleteShader);
}

void Game::displayTest()
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	float tempMat[16];
	
	glUseProgram(theProgram);
	glBindVertexArray(vao);

	yRot.rotate(0, 0, 1, 90);
	yRot.quaternionToMatrix().getMat4x4(tempMat);
	
	modelMatrix.identityMatrix();
	modelMatrix.translationMatrix(lightPosition.x, lightPosition.y, lightPosition.z);
	modelMatrix.scaleMatrix(0.2f, 0.2f, 0.2f);
	modelMatrix.getMat4x4(tempMat);
	glUniformMatrix4fv(modelMatrixUnif, 1, GL_FALSE, tempMat);
	
	glDrawElements(GL_TRIANGLES, indiceCount, GL_UNSIGNED_SHORT, 0);
	Vector3D tempLight;
	Matrix4x4 temp = modelMatrix * viewMatrix;
	tempLight = lightPosition * temp;
	temp.inverse();
	tempLight = tempLight * temp;
	
	glUniform3f(lightPositionUnif, tempLight.x, tempLight.y, tempLight.z);

	//modelMatrix.translationMatrix(0, 0.5, 0);	
	//modelMatrix = modelMatrix * yRot.quaternionToMatrix();
	modelMatrix.identityMatrix();
	//modelMatrix.scaleMatrix(0.5, 0.5, 0.5);
	modelMatrix.getMat4x4(tempMat);

//	std::cout << "X " << lightPosition.x << std::endl;
//	std::cout << "Y " << lightPosition.y << std::endl;
//	std::cout << "Z " << lightPosition.z << std::endl;


	glUniformMatrix4fv(modelMatrixUnif, 1, GL_FALSE, tempMat);
	glDrawElements(GL_TRIANGLES, indiceCount, GL_UNSIGNED_SHORT, 0);
	

	
	// adds on an offset/bias onto the indices before drawing I.E 0 turns to 18 etc.
	//glDrawElementsBaseVertex(GL_TRIANGLES, 24, GL_UNSIGNED_SHORT, 0, 36 / 2);
    
	/*
	// old draw code
	glBindBuffer(GL_ARRAY_BUFFER, colourBuffer);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, positionBufferObject);
	// enable vertex shader index 0 (position for testShader)
	glEnableVertexAttribArray(0);
	// 1st param attrib index 2nd param how many values represent a single piece of data, 3rd type of data, 
	// 5th how much spacing between values, 6th byte offset from the value in the buffer object at the front
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	*/

}

void Game::setup_RC(int depthBits, int stencilBits, int antiAliasing, int majorC, int minorC)
{
	// setting up context settings, depth buffer bits, stencil bits etc.
	sf::ContextSettings settings;
	settings.depthBits = 24;
	settings.stencilBits = 8;
	settings.antialiasingLevel = 4;
	settings.majorVersion = 4;
	settings.minorVersion = 0;

	// creating window, and GL context
	window.create(sf::VideoMode(800, 600), "OpenGL", sf::Style::Default, settings);

	// turning vertical frame rate synchronization on, so the app runs at the
	// same rate as the monitor
	window.setVerticalSyncEnabled(true);

	// initilizing glew for using gl
	GLenum err = glewInit();

	if (GLEW_OK != err)
	{
	/* Problem: glewInit failed, something is seriously wrong. */
	fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
	}
	
	fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));
	
	std::cout << "depth bits:" << settings.depthBits << std::endl;
	std::cout << "stencil bits:" << settings.stencilBits << std::endl;
	std::cout << "antialiasing level:" << settings.antialiasingLevel << std::endl;
	std::cout << "version:" << settings.majorVersion << "." << settings.minorVersion << std::endl;
}

void Game::update()
{
	float tempMat[16];
	viewMatrix.viewMatrix(eye, up, at);
	viewMatrix.getMat4x4(tempMat);
	glUniformMatrix4fv(viewMatrixUnif, 1, GL_FALSE, tempMat);
//	glBindBuffer(GL_UNIFORM_BUFFER, globalMatricesUBO);
//	glBufferSubData(GL_UNIFORM_BUFFER, sizeof(float)*16, sizeof(float)*16, tempMat);
//	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void Game::run()
{
	// passing in buffer values, GL_context etc 
	setup_RC(24, 8, 4, 4, 0);

	
	
	initProgram("perFragmentDiffuse.vert","perFragmentDiffuse.frag");
	initializeVertexBuffer();
	initializeVertexArrayObject();

	while(window.isOpen())
	{
		sf::Event event;
				
		while(window.pollEvent(event))
		{
			if(event.type == sf::Event::Closed)
				window.close();
			
			if(event.type == sf::Event::Resized)
				reshape(event.size.width, event.size.height);
			
			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::W))
			{
				eye = UsefulFunctions::moveForward(eye, 0.1, angle);
				at = UsefulFunctions::moveForward(eye, 1.0, angle);
			}

			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::S))
			{
				eye = UsefulFunctions::moveForward(eye, -0.1, angle);
				at = UsefulFunctions::moveForward(eye, 1.0, angle);
			}

			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::A))
			{
				eye = UsefulFunctions::moveRight(eye, -0.1, angle);
				at = UsefulFunctions::moveForward(eye, 1.0, angle);
			}

			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::D))
			{
				eye = UsefulFunctions::moveRight(eye, 0.1, angle);
				at = UsefulFunctions::moveForward(eye, 1.0, angle);
			}

			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Comma))
			{
				angle -= 1;
				at = UsefulFunctions::moveForward(eye, 1.0, angle);
			}

			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Period))
			{
				angle += 1;
				at = UsefulFunctions::moveForward(eye, 1.0, angle);
			}

			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::X))
			{
				lightPosition.x += 0.1;
			}

			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Z))
			{
				lightPosition.x -= 0.1;
			}

			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::C))
			{
				lightPosition.y -= 0.1;
			}

			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::V))
			{
				lightPosition.y += 0.1;
			}
		}

		update();
		displayTest();
		window.display();
	}
	
}

Game::~Game()
{
}
