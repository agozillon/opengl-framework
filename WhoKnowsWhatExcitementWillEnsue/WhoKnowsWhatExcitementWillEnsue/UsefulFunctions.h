#ifndef USEFULFUNCTIONS_H
#define USEFULFUNCTIONS_H
#include "Vector3D.h"

namespace UsefulFunctions{
	Vector3D moveForward(Vector3D vector, float direction, float angle);
	Vector3D moveRight(Vector3D vector, float direction, float angle);
}

#endif