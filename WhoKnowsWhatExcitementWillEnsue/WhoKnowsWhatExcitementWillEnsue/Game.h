#ifndef GAME_H
#define GAME_H
#include <GL\glew.h>
#include <SFML\OpenGL.hpp>
#include <SFML\Window.hpp>
#include <string>
#include <fstream>
#include "Vector3D.h"
#include "Matrix4x4.h"
#include "Quaternion.h"

class Game{
public:
	void run();
	Game();
	~Game();

private:
	void setup_RC(int depthBits, int stencilBits, int antiAliasing, int majorC, int minorC);	
	void displayTest();
	void initializeVertexBuffer();
	void reshape(int w, int h);
	void initProgram(char * strVertShader, char * strFragShader);
	void initializeVertexArrayObject();
	void update();
	GLuint createProgram(const std::vector<GLuint> &shaderList);
	GLuint createShader(GLenum eShaderType, const std::string &strShaderFile);
	std::string fileToString(char * fileName);

	GLuint vertexBufferObject;
	GLuint colourBuffer;
	GLuint theProgram;
	GLuint vao;
	GLuint vao2;
	GLuint indexBufferObject;
	GLuint normBufferObject;

	Matrix4x4 projectionMatrix;
	Matrix4x4 modelMatrix;
	Matrix4x4 viewMatrix;

	//int globalMatricesBindingIndex;

	//GLuint globalMatricesUnifBlock;
	//GLuint globalMatricesUBO;
	GLuint viewMatrixUnif;
	GLuint perspectiveMatrixUnif;
	GLuint modelMatrixUnif;
	GLuint lightPositionUnif;
	GLuint lightIntensityUnif;
	GLuint ambientUnif;
	GLuint albedoUnif;
	GLuint roughnessUnif;

	Vector3D eye;  
	Vector3D at;
	Vector3D up;

	Vector3D lightPosition;
	Vector3D lightIntensity;

	Quaternion xRot;
	Quaternion yRot;
	Quaternion zRot;
	float angle;
	int indiceCount;
	sf::Window window;
};

#endif