#ifndef VECTOR3D_H
#define VECTOR3D_H

class Matrix4x4;

class Vector3D{
private:
	
public:
	Vector3D(){}
	Vector3D (float, float, float);
	Vector3D (int, int, int);
	Vector3D operator+ (const Vector3D);
	Vector3D operator- (const Vector3D);
	Vector3D operator* (const int);
	Vector3D operator- ();
	Vector3D operator*(Matrix4x4);
	Vector3D normalize();
	float magnitude();
	float dotProduct (const Vector3D);
	Vector3D crossProduct (const Vector3D);

	float x, y, z;
};

#endif