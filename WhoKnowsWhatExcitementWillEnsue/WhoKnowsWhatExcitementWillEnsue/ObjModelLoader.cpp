#include "ObjModelLoader.h"

#include <sstream>
#include <iostream>
#include <fstream>

namespace ObjModelLoader{
	std::string openFile(char * fname, int &nOfVerts, int &nOfNorms, int &nOfIndices, int &nOfTexture)
	{
		std::string temp;
		std::fstream myfile;
		myfile.open(fname, std::ios::in);

		if(myfile.is_open())
		{
			while(!myfile.eof())
			{
			
				char c = myfile.get();

				if(c == 'v' && myfile.peek() == ' ' || c == 'v' && isdigit(myfile.peek()) > 0)
				{
					nOfVerts += 3;
				}
				else if(c == 'v' && myfile.peek() == 'n')
				{
					nOfNorms += 3;
				}
				else if(c == 'v' && myfile.peek() == 't')
				{
					nOfTexture += 2;
				}
				else if(c == 'f' && myfile.peek() == ' ') // not really required just a slight failsafe incase the obj header contains an f
				{
					nOfIndices += 3;
				}
				
				if(myfile.good())
				 temp+=c;
			}
		}
		else
			std::cout << "file couldn't be opened";
		
	//	std::cout << temp << std::endl;
		/*
		myfile.close();

		myfile.open("testdragon.txt");
		myfile.write(temp.c_str(), temp.size()-1);
		myfile.close();
		*/
		return temp;
	}

	void rearrangeNormals(std::vector<float> &norm, std::vector<int> normIndices)
	{
		std::vector<float> temp;

		temp.reserve(normIndices.size() * 3);


		for(int i = 0; i < normIndices.size(); i++)
		{
			for(int i2 = 3; i2 > 0; i2--)
			{
				temp.push_back(norm.data()[(normIndices.data()[i]*3)-i2]);
			}
		}

		norm = temp;
	}

	void getValues(std::string objFile, std::vector<float> &vert, std::vector<int> &indice, std::vector<float> &norm, std::vector<float> &texCo, bool hasTexCo, bool hasNorms)
	{
		int vCounter = 0, iCounter = 0, nCounter = 0, tCounter = 0;
		std::vector<int> normIndice;

		for(long int i = 0; i < objFile.size(); i++)
		{
			if(objFile[i] == 'v' && objFile[i+1] == ' ')
			{
				long int n = i + 2;
				int nValues = 0;
				
				while(nValues < 3)
				{
					std::string vertex;
				
					while(objFile[n] != 'v'  && objFile[n] != ' ' && (objFile[n] !=  '\n' && (isdigit(objFile[n+1]) > 0 || objFile[n+1] == '-' || objFile[n+1] =='+' || objFile[n+1] == '.')))
					{
						if(objFile[n] == '+' || objFile[n] == '-' || objFile[n] == '.' || isdigit(objFile[n]) > 0)
							vertex += objFile[n];
						
						n++;
					}

					if(!vertex.empty())
					{
						//strtod(vertex.c_str(), NULL)

						vert.push_back(atof(vertex.c_str()));
						nValues ++;
					}

					 n++;
				}

				// speeds up sorting by moving main iterator to the last
				// check point -1, -1 because just n can cause it to skip full vertices.
				i = n-1;
			}
			else if(objFile[i] == 'v' && objFile[i+1] == 'n')
			{
				long int n = i + 3;
				int nValues = 0;
				
				while(nValues < 3)
				{
					std::string normal;
				
					while(objFile[n] != 'v' && objFile[n] != ' ' && objFile[n] !=  '\n')
					{
						if(objFile[n] == '+' || objFile[n] == '-' || objFile[n] == '.' || isdigit(objFile[n]) > 0)
						   normal += objFile[n];
					
						n++;
					}
					
					if(!normal.empty())
					{
						norm.push_back(atof(normal.c_str()));
						nValues++;
					}

					n++;
				}
				
					i = n - 1;
			}
			else if(objFile[i] == 'v' && objFile[i+1] == 't')
			{
				long int n = i + 2;
				int nValues = 0;
				
				while(nValues < 2)
				{
					std::string texcoord;
				
					while(objFile[n] != 'v'  && objFile[n] != ' ' && (objFile[n] !=  '\n' && (isdigit(objFile[n+1]) > 0 || objFile[n+1] == '-' || objFile[n+1] =='+' || objFile[n+1] == '.')))
					{
						if(objFile[n] == '+' || objFile[n] == '-' || objFile[n] == '.' || isdigit(objFile[n]) > 0)
							texcoord += objFile[n];
						
						n++;
					}

					if(!texcoord.empty())
					{
						texCo.push_back(atof(texcoord.c_str()));
						nValues ++;
					}

					 n++;
				}

				// speeds up sorting by moving main iterator to the last
				// check point -1, -1 because just n can cause it to skip full vertices.
				i = n-1;
			}
			else if(objFile[i] == 'f' && objFile[i+1] == ' ')
			{
				
				long int n = i + 2;
				int nValues = 0;

				
				while(nValues < 3)
				{
					std::string indiceString;

					// removed '\n' from this check to try fix the dragon
					while(n < objFile.size()-1 && objFile[n] != '/'  && objFile[n] !=  'f' && objFile[n] != ' ')
					{
	
						if(objFile[n] == '+' || objFile[n] == '-' || objFile[n] == '.' || isdigit(objFile[n]) > 0)
						indiceString += objFile[n];
						
						n++;
					}
					
		
					
					if(!indiceString.empty())
					{
						indice.push_back(atoi(indiceString.c_str()) - 1);

						nValues++;
						
						indiceString.clear();

						if(hasNorms && !hasTexCo)
						{
							//n+=3;

							while(isdigit(objFile[n]) > 0 || objFile[n] == '/')
							{
								if(objFile[n] == '+' || objFile[n] == '-' || objFile[n] == '.' || isdigit(objFile[n]) > 0)
									indiceString += objFile[n];
								
								n++;
							}

							normIndice.push_back(atoi(indiceString.c_str()));
						}
						else if(hasNorms && hasTexCo && nValues < 3)
						{
							n+=4;

							while(objFile[n] != ' ')
							{
								n++;
							}
						}
						else if(!hasNorms && hasTexCo && nValues < 3)
						{
							n+=2;

							while(objFile[n] != ' ')
							{
								n++;
							}
						}
					}
					
					if(indiceString.empty())
						n++;
				}

				i = n - 1;

				
			}	
		}

		rearrangeNormals(norm, normIndice);
	}

	void contentCheck(char * fname, std::vector<int> &indices, int  &indiceCount, std::vector<float> &vertices, std::vector<float> &normals, std::vector<float> &texCoords)
	{
		int nVerts = 0, nIndices = 0, nNorms = 0, tCoords = 0;
		bool tex = false, norms = false;

		std::string file;
		file = openFile(fname, nVerts, nNorms, nIndices, tCoords);
		
		indices.reserve(nIndices);
		vertices.reserve(nVerts);
		normals.reserve(nNorms);
		texCoords.reserve(tCoords);

		indiceCount = nIndices;

		if(tCoords > 0)
		{
			tex = true;
		}

		if(nNorms > 0)
		{
			norms = true;
		}
		
		getValues(file, vertices, indices, normals, texCoords, tex, norms);
		/*
		for(int i = indices.size()-20; i < indices.size(); i++)
		{
			std::cout << indices.data()[i] << std::endl; 
		}
		*/
	/*	
		//std::string fuckthis;
		std::fstream myfile;
		std::stringstream o;

		for(int i = 3; i < vertices.size()+3; i++)
		{
			if(i % 3 == 0)
			{
				o << 'v';
				o << " ";
			}

			o << vertices.data()[i-3];
			o << " ";
		}

		myfile.open("test2.txt");
		myfile.write(o.str().c_str(), o.str().size());
		myfile.close();
		*/
	}

}