#ifndef MATRIX4X4_H
#define MATRIX4X4_H
#include "Vector3D.h"
#include <vector>
#include <iomanip>

class Matrix4x4{
private:
	float mat4x4[16];
//	float stackOfMat4x4;
//	int sizeOfStack;
public:
	Matrix4x4(float mat[]){for(int i = 0; i < 16; i++){mat4x4[i] = mat[i];}}
	Matrix4x4(){identityMatrix();}
	void projectionMatrix(float frustumScale, float aspectRatio, float fzNear, float fzFar);
	void translationMatrix(float x, float y, float z);
	void rotationMatrix(float axisX, float axisY, float axisZ, float angle);
	void scaleMatrix(float scaleX, float scaleY, float scaleZ);
	void identityMatrix();
	void inverseTranspose();
	void inverse();
	Matrix4x4 operator*(Matrix4x4);
	void viewMatrix(Vector3D eye, Vector3D up, Vector3D at);
	void getMat4x4(float temp[]){ memcpy(temp, mat4x4, sizeof(float) * 16);}
//	void popMatrix();
//	void pushMatrix();
};

#endif