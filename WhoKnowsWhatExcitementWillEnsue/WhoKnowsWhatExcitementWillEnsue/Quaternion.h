#ifndef QUATERNION_H
#define QUATERNION_H
#include "Matrix4x4.h"

class Quaternion{
public:
	Quaternion(){i = 0; j = 0; k = 0; s = 1;} 
	Quaternion(float I, float J, float K, float S){i = I; j = J; k = K; s = S;} 
	void rotate(float x, float y, float z, float a);
	void normalize();
	Quaternion operator*(Quaternion);
	Matrix4x4 quaternionToMatrix();

private:
	float i, j, k, s, angle;
};
#endif