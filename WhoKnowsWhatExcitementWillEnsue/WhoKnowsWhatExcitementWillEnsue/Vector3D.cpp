#include "Vector3D.h"
#include <math.h>
#include "Matrix4x4.h"

Vector3D::Vector3D (float i, float j, float k)
{
	x = i;	y = j; z = k;
}

Vector3D::Vector3D (int i, int j, int k)
{
	x = i; y = j; z = k;
}

Vector3D Vector3D::operator+ (const Vector3D param)
{
	return Vector3D((x + param.x),(y + param.y),(z + param.z));
}

Vector3D Vector3D::operator- (const Vector3D param)
{
	return Vector3D((x - param.x),(y - param.y),(z - param.z));
}

Vector3D Vector3D::operator* (const int scalar)
{
	return Vector3D((x * scalar), (y * scalar), (z * scalar));
}

Vector3D Vector3D::operator- ()
{
	return Vector3D(-x,	-y, -z);
}

float Vector3D::magnitude()
{
	return sqrtf((x*x) + (y*y) + (z*z));
}
	
float Vector3D::dotProduct (const Vector3D param)
{
	return sqrtf((x * param.x) + (y * param.y) + (z * param.z));
}

Vector3D Vector3D::normalize()
{
	float divisor = magnitude();
	return Vector3D((x / divisor), (y / divisor), (z / divisor));
}

Vector3D Vector3D::operator*(Matrix4x4 param)
{
	float temp[16];
	Vector3D tempVec;
	param.getMat4x4(temp);

	tempVec.x = temp[0] * tempVec.x + temp[4] * tempVec.y + temp[8] * tempVec.z + temp[12] * 1;
	tempVec.y = temp[1] * tempVec.x + temp[5] * tempVec.y + temp[9] * tempVec.z + temp[13] * 1;
	tempVec.z = temp[2] * tempVec.x + temp[6] * tempVec.y + temp[10] * tempVec.z + temp[14] * 1;

	return tempVec;
}

Vector3D Vector3D::crossProduct (const Vector3D param)
{
	return Vector3D((y * param.z - z * param.y), (z * param.x - x * param.z), (x * param.y - y * param.x));
}
