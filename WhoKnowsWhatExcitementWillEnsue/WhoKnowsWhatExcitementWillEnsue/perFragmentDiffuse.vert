#version 330

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;

out vec4 oColor;
out vec3 oNormal;
out vec3 oMSpacePosition;

uniform mat4 modelMatrix;
uniform mat4 perspectiveMatrix;
uniform mat4 viewMatrix;

void main()
{
	oMSpacePosition = position;
	gl_Position = perspectiveMatrix * viewMatrix * modelMatrix * vec4(position, 1.0);
	oNormal = normal;
	oColor = vec4(1.0, 0.0, 0.0, 1.0);
}
