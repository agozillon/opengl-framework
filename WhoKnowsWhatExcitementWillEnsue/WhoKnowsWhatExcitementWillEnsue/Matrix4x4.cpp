#include "Matrix4x4.h"
#include <iostream>
#include <math.h>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/type_ptr.hpp>


#define degToRad 0.017453293f

void Matrix4x4::projectionMatrix(float fov, float aspectRatio, float fzNear, float fzFar) 
{
	memset(mat4x4, 0, sizeof(float) * 16);
	float frustumScale =  1.0f / tan((fov * degToRad)/ 2.0f);
	mat4x4[0] = frustumScale / aspectRatio;
	mat4x4[5] = frustumScale;
	mat4x4[10] = (fzFar + fzNear) / (fzNear - fzFar);
	mat4x4[14] = (2 * fzFar * fzNear) / (fzNear - fzFar);
	mat4x4[11] = -1.0f;
}

void Matrix4x4::translationMatrix(float x, float y, float z)
{
	mat4x4[12] = (mat4x4[0] * x) + (mat4x4[12] * 1);
	mat4x4[13] = (mat4x4[5] * y) + (mat4x4[13] * 1);
	mat4x4[14] = (mat4x4[10] * z) + (mat4x4[14] * 1);
	mat4x4[15] = 1.0f;
}

void Matrix4x4::rotationMatrix(float axisX, float axisY, float axisZ, float angle)
{
	float radAngle = angle * degToRad;
	float tempArray[16];
	memcpy(tempArray, mat4x4, sizeof(float) * 16);

	// rotation around x Axis
	if(axisX > 0 && axisY == 0 && axisZ == 0)
	{
		mat4x4[4] = (tempArray[4] * cos(radAngle)) + (tempArray[8] * sin(radAngle));
		mat4x4[5] = (tempArray[5] * cos(radAngle)) + (tempArray[9] * sin(radAngle));
		mat4x4[6] = (tempArray[6] * cos(radAngle)) + (tempArray[10] * sin(radAngle));
		mat4x4[7] = (tempArray[7] * cos(radAngle)) + (tempArray[11] * sin(radAngle));

		mat4x4[8] = (tempArray[4] * -sin(radAngle)) + (tempArray[8] * cos(radAngle));
		mat4x4[9] = (tempArray[5] * -sin(radAngle)) + (tempArray[9] * cos(radAngle));
		mat4x4[10] = (tempArray[6] * -sin(radAngle)) + (tempArray[10] * cos(radAngle));
		mat4x4[11] = (tempArray[7] * -sin(radAngle)) + (tempArray[11] * cos(radAngle));
	}
		
	// rotation around y axis
	if(axisX == 0 && axisY > 0 && axisZ == 0)
	{
		mat4x4[0] = (tempArray[0] * cos(radAngle)) + (tempArray[8] * -sin(radAngle));
		mat4x4[1] = (tempArray[1] * cos(radAngle)) + (tempArray[9] * -sin(radAngle));
		mat4x4[2] = (tempArray[2] * cos(radAngle)) + (tempArray[10] * -sin(radAngle));
		mat4x4[3] = (tempArray[3] * cos(radAngle)) + (tempArray[11] * -sin(radAngle));

		mat4x4[8] = (tempArray[0] * sin(radAngle)) + (tempArray[8] * cos(radAngle));
		mat4x4[9] = (tempArray[1] * sin(radAngle)) + (tempArray[9] * cos(radAngle));
		mat4x4[10] = (tempArray[2] * sin(radAngle)) + (tempArray[10] * cos(radAngle));
		mat4x4[11] = (tempArray[3] * sin(radAngle)) + (tempArray[11] * cos(radAngle));
	}
		
	// rotation around z axis
	if(axisX == 0 && axisY == 0 && axisZ > 0)
	{
		mat4x4[0] = (tempArray[0] * cos(radAngle)) + (tempArray[4] * sin(radAngle));
		mat4x4[1] = (tempArray[1] * cos(radAngle)) + (tempArray[5] * sin(radAngle));
		mat4x4[2] = (tempArray[2] * cos(radAngle)) + (tempArray[6] * sin(radAngle));
		mat4x4[3] = (tempArray[3] * cos(radAngle)) + (tempArray[7] * sin(radAngle));

		mat4x4[4] = (tempArray[0] * -sin(radAngle)) + (tempArray[4] * cos(radAngle)); 
		mat4x4[5] = (tempArray[1] * -sin(radAngle)) + (tempArray[5] * cos(radAngle));
		mat4x4[6] = (tempArray[2] * -sin(radAngle)) + (tempArray[6] * cos(radAngle));
		mat4x4[7] = (tempArray[3] * -sin(radAngle)) + (tempArray[7] * cos(radAngle));
	}
}

	
void Matrix4x4::scaleMatrix(float scaleX, float scaleY, float scaleZ)
{
	mat4x4[0] = (scaleX * mat4x4[0]);
	mat4x4[4] = (scaleX * mat4x4[4]);
	mat4x4[8] = (scaleX * mat4x4[8]);
	mat4x4[12] = (scaleX * mat4x4[12]);

	mat4x4[1] = (scaleY * mat4x4[1]);
	mat4x4[5] = (scaleY * mat4x4[5]);
	mat4x4[9] = (scaleY * mat4x4[9]);
	mat4x4[13] = (scaleY * mat4x4[13]);
	
	mat4x4[2] = (scaleZ * mat4x4[2]);
	mat4x4[6] = (scaleZ * mat4x4[6]);
	mat4x4[10] = (scaleZ * mat4x4[10]);
	mat4x4[14] = (scaleZ * mat4x4[14]);
}

void Matrix4x4::identityMatrix()
{
	memset(mat4x4, 0, sizeof(float) * 16);

	mat4x4[0] = 1.0f;
	mat4x4[5] = 1.0f;
	mat4x4[10] = 1.0f;
	mat4x4[15] = 1.0f;
}

void Matrix4x4::viewMatrix(Vector3D eye, Vector3D up, Vector3D at)
{
	Vector3D camZ = eye - at;
		
	Vector3D nCamZ = camZ.normalize();

	Vector3D camX(0,0,0);

	camX = up.crossProduct(camZ);
	Vector3D nCamX = camX.normalize(); 

	Vector3D camY(0,0,0);
	camY = nCamZ.crossProduct(nCamX);

	glm::mat4x4 temp(nCamX.x, nCamX.y, nCamX.z, 0, camY.x, camY.y, camY.z, 0, nCamZ.x, nCamZ.y, nCamZ.z, 0, eye.x, eye.y, eye.z, 1);
	temp = glm::inverse(temp);	

	float * tMat = glm::value_ptr(temp);
		
	for(int i = 0; i < 16; i++)
	{
		mat4x4[i] = tMat[i];
	}
}

void Matrix4x4::inverse()
{
	glm::mat4x4 temp(mat4x4[0], mat4x4[1], mat4x4[2], mat4x4[3], mat4x4[4], mat4x4[5], mat4x4[6], mat4x4[7], mat4x4[8], mat4x4[9], mat4x4[10], mat4x4[11], mat4x4[12], mat4x4[13], mat4x4[14], mat4x4[15]);
	
	temp = glm::inverse(temp);	

	float * tMat = glm::value_ptr(temp);
		
	for(int i = 0; i < 16; i++)
	{
		mat4x4[i] = tMat[i];
	}
}

void Matrix4x4::inverseTranspose()
{
	glm::mat4x4 temp(mat4x4[0], mat4x4[1], mat4x4[2], mat4x4[3], mat4x4[4], mat4x4[5], mat4x4[6], mat4x4[7], mat4x4[8], mat4x4[9], mat4x4[10], mat4x4[11], mat4x4[12], mat4x4[13], mat4x4[14], mat4x4[15]);
	
	temp = glm::transpose(glm::inverse(temp));	

	float * tMat = glm::value_ptr(temp);
		
	for(int i = 0; i < 16; i++)
	{
		mat4x4[i] = tMat[i];
	}
}

Matrix4x4 Matrix4x4::operator*(Matrix4x4 pMat)
{
	float param[16];
	float temp[16];
	pMat.getMat4x4(param);

	temp[0] = mat4x4[0] * param[0] + mat4x4[4] * param[1] + mat4x4[8] * param[2] + mat4x4[12] * param[3];
	temp[4] = mat4x4[0] * param[4] + mat4x4[4] * param[5] + mat4x4[8] * param[6] + mat4x4[12] * param[7];
	temp[8] = mat4x4[0] * param[8] + mat4x4[4] * param[9] + mat4x4[8] * param[10] + mat4x4[12] * param[11];
	temp[12] = mat4x4[0] * param[12] + mat4x4[4] * param[13] + mat4x4[8] * param[14] + mat4x4[12] * param[15];
	
	temp[1] = mat4x4[1] * param[0] + mat4x4[5] * param[1] + mat4x4[9] * param[2] + mat4x4[13] * param[3];
	temp[5] = mat4x4[1] * param[4] + mat4x4[5] * param[5] + mat4x4[9] * param[6] + mat4x4[13] * param[7];
	temp[9] = mat4x4[1] * param[8] + mat4x4[5] * param[9] + mat4x4[9] * param[10] + mat4x4[13] * param[11];
	temp[13] = mat4x4[1] * param[12] + mat4x4[5] * param[13] + mat4x4[9] * param[14] + mat4x4[13] * param[15];

	temp[2] = mat4x4[2] * param[0] + mat4x4[6] * param[1] + mat4x4[10] * param[2] + mat4x4[14] * param[3];
	temp[6] = mat4x4[2] * param[4] + mat4x4[6] * param[5] + mat4x4[10] * param[6] + mat4x4[14] * param[7];
	temp[10] = mat4x4[2] * param[8] + mat4x4[6] * param[9] + mat4x4[10] * param[10] + mat4x4[14] * param[11];
	temp[14] = mat4x4[2] * param[12] + mat4x4[6] * param[13] + mat4x4[10] * param[14] + mat4x4[14] * param[15];

	temp[3] = mat4x4[3] * param[0] + mat4x4[7] * param[1] + mat4x4[11] * param[2] + mat4x4[15] * param[3];
	temp[7] = mat4x4[3] * param[4] + mat4x4[7] * param[5] + mat4x4[11] * param[6] + mat4x4[15] * param[7];
	temp[11] = mat4x4[3] * param[8] + mat4x4[7] * param[9] + mat4x4[11] * param[6] + mat4x4[15] * param[11];
	temp[15] = mat4x4[3] * param[12] + mat4x4[7] * param[13] + mat4x4[11] * param[14] + mat4x4[15] * param[15];

	return Matrix4x4(temp);
}


